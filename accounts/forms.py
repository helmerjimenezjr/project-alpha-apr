from django import forms
from projects.models import Project
from django.contrib.auth.models import User


class LogInForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(),
    )


class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )
    password_confirmation = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )


class ProjectForm(forms.ModelForm):
    name = forms.CharField(max_length=150)
    description = forms.CharField(widget=forms.Textarea)
    owner = forms.ModelChoiceField(queryset=User.objects.all())

    class Meta:
        model = Project
        fields = [
            "name",
            "description",
            "owner",
        ]
